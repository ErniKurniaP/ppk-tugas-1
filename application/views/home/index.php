<div class="jumbotron">
<div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
  <div class="carousel-inner">
    <div class="carousel-item active">
        <br>
      <img src="https://upload.wikimedia.org/wikipedia/commons/thumb/8/84/Tempo_Magazine.svg/1200px-Tempo_Magazine.svg.png" class="d-block w-100" alt="Logo Tempo" style="max-height: 300px; max-width : 400px;">
        <br> <br> <br>
    </div>
    <div class="carousel-item">
      <img src="https://upload.wikimedia.org/wikipedia/en/thumb/e/eb/CNN_Indonesia.svg/1200px-CNN_Indonesia.svg.png" class="d-block w-100" alt="Logo CNN" style="max-height: 200px; max-width: 144px;">
        <br> <br>
    </div>
    <div class="carousel-item">
        <br> <br>
      <img src="https://media-exp1.licdn.com/dms/image/C561BAQHNdANzjETFvQ/company-background_10000/0/1595958031460?e=2147483647&v=beta&t=QQ-wnU8ibbM9SOntDwGbcx_rLjKPRf0tO1H3HB_6HNM" class="d-block w-100" alt="Logo Republika.co.id" style="max-height: 675px; max-width: 540px;">
        <br> <br> <br>
    </div>
    <div class="carousel-item">
      <img src="https://cdn.bmkg.go.id/Web/Logo-BMKG-new.png" class="d-block w-100" alt="Logo BMKG" style="max-height: 170px; max-width: 116px;">
        <br><br>
    </div>
  </div>
 <button class="carousel-control-prev" type="button" data-target="#carouselExampleControls" data-slide="prev">
    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
    <span class="sr-only">Previous</span>
  </button>
  <button class="carousel-control-next" type="button" data-target="#carouselExampleControls" data-slide="next">
    <span class="carousel-control-next-icon" aria-hidden="true"></span>
    <span class="sr-only">Next</span>
  </button>
</div>
  <h1 class="display-4">Selamat Datang di Portal Berita!</h1>
  <p class="lead">Website ini menampilkan RSS berita dari berbagai sumber yang diterjemahkan dari XML dan JSON</p>
  <hr class="my-4">
  
  <p>Pemrograman Platform Khusus, Politeknik Statistika STIS </p>
  <br> <br> 
  <span class="breadcrumb-item font-italic font-weight-light">Developed by Erni Kurnia Putri (222011792) </span>
  <br>
  <a href="https://www.instagram.com/rr.ernii/"><img src="https://upload.wikimedia.org/wikipedia/commons/thumb/9/95/Instagram_logo_2022.svg/2048px-Instagram_logo_2022.svg.png" alt="Logo Instagram" style="max-height: 15px; max-width: 15px;"></a>
  <a href="https://wa.me/085878807483"><img src="https://www.tanjunglesung.com/wp-content/uploads/2018/12/logo-wa-whatsapp-300x300.png" alt="Logo Whatsapp" style="max-height: 20px; max-width: 20px;"></a>
  <a href="https://mail.google.com/mail/u/0/?view=cm&tf=1&fs=1&to=222011792@stis.ac.id"><img src="https://upload.wikimedia.org/wikipedia/commons/thumb/7/7e/Gmail_icon_%282020%29.svg/2560px-Gmail_icon_%282020%29.svg.png" alt="Logo Gmail" style="max-height: 15px; max-width: 15px;"></a>
</div>
