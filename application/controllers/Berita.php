<?php

class Berita extends CI_Controller {
    public function index() {
        $this->load->view('templates/header');
        $this->load->view('home/index');
        $this->load->view('templates/footer');
    }

    public function berita1() {
        $this->load->view('templates/header');
        $berita_xml_1 = file_get_contents('https://rss.tempo.co/bisnis');
        $xml_1 = simplexml_load_string($berita_xml_1);
        $data['berita_xml_1'] = $xml_1;
        $this->load->view('berita1/index', $data);
        $this->load->view('templates/footer');
    }

    public function berita2() {
        $this->load->view('templates/header');
        $berita_xml_2 = file_get_contents('https://www.cnnindonesia.com/ekonomi/rss');
        $xml_2 = simplexml_load_string($berita_xml_2);
        $data['berita_xml_2'] = $xml_2;
        $this->load->view('berita2/index', $data);
        $this->load->view('templates/footer');
    }

    public function berita3() {
        $this->load->view('templates/header');
        $berita_xml_3 = file_get_contents('https://www.republika.co.id/rss');
        $xml_3 = simplexml_load_string($berita_xml_3);
        $data['berita_xml_3'] = $xml_3;
        $this->load->view('berita3/index', $data);
        $this->load->view('templates/footer');
    }

    public function berita4() {
        $this->load->view('templates/header');
        $berita_json = file_get_contents('https://data.bmkg.go.id/DataMKG/TEWS/gempaterkini.json');
        $json_port = json_decode($berita_json, true);
        //Dapatkan 15 data terakhir
        $data['berita_json'] = $json_port;
        // echo "<pre>";
        // print_r($json_port);
        $this->load->view('berita4/index', $data);
        $this->load->view('templates/footer');
    }

}